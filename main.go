package main

import (
	"encoding/base64"
	"fmt"
	"net/http"
	"os"
	"strings"

	"github.com/xanzy/go-gitlab"
)

const (
	gitlabRunnerMaintainersGroupID = 7425590
	requiredAccessLevel            = int(gitlab.MaintainerPermissions)
	glPATEnvVarName                = "GLPAT"
)

func main() {
	envVarValue := os.Getenv(glPATEnvVarName)

	if envVarValue == "" {
		panic(fmt.Sprintf("Environment variable '%s' is not set.\n", glPATEnvVarName))
	}

	git, err := gitlab.NewClient(envVarValue)
	if err != nil {
		panic(err)
	}
	problems := make([]string, 0)

	repositories := []struct {
		RepositoryName string
	}{{
		RepositoryName: "gitlab-org/ci-cd/shared-runners/images/gcp/windows-containers",
	}, {
		RepositoryName: "gitlab-org/ci-cd/shared-runners/images/macstadium/orka",
	}, {
		RepositoryName: "gitlab-org/ci-cd/shared-runners/macos",
	}, {
		RepositoryName: "gitlab-com/gl-infra/ci-runners/deployer",
	}, {
		RepositoryName: "gitlab-com/gl-infra/ci-runners/k8s-workloads",
	}, {
		RepositoryName: "gitlab-cookbooks/cookbook-gitlab-runner",
	}, {
		RepositoryName: "gitlab-cookbooks/cookbook-wrapper-gitlab-runner",
	}, {
		RepositoryName: "gitlab-org/gitlab-runner",
	}, {
		RepositoryName: "gitlab-org/fleeting/taskscaler",
	}, {
		RepositoryName: "gitlab-org/fleeting/fleeting",
	}, {
		RepositoryName: "gitlab-org/fleeting/fleeting-plugin-aws",
	}, {
		RepositoryName: "gitlab-org/fleeting/fleeting-plugin-googlecompute",
	}, {
		RepositoryName: "gitlab-org/fleeting/fleeting-plugin-azurecompute",
	}}

	for _, r := range repositories {
		if !repoExists(r.RepositoryName) {
			problems = append(problems, fmt.Sprintf("%s: Repository does not exist or is not accessible\n", r.RepositoryName))
			continue
		}

		project, _, err := git.Projects.GetProject(r.RepositoryName, &gitlab.GetProjectOptions{})
		if err != nil {
			continue
		}

		codeOwnersSet, err := checkCodeowners(git, r.RepositoryName)
		if err != nil {
			problems = append(problems, fmt.Sprintf("%s: Could not check CODEOWNERS: %v\n", r.RepositoryName, err))
		}
		if !codeOwnersSet {
			problems = append(problems, fmt.Sprintf("%s: CODEOWNERS not set\n", r.RepositoryName))
		}

		if !checkGroupAccessLevel(project) {
			problems = append(problems, fmt.Sprintf("%s: Insufficient access level for @gitlab-com/runner-maintainers. Grant MaintainerPermissions in project\n", r.RepositoryName))
		}

		if !checkRequestAccess(project) {
			problems = append(problems, fmt.Sprintf("%s: RequestAccessEnabled is allowed on project. Should be turned off\n", r.RepositoryName))
		}
	}

	if len(problems) == 0 {
		fmt.Println("Correct settings are configured in all projects")
		return
	}

	for _, p := range problems {
		fmt.Fprintf(os.Stderr, p)
	}
}

func repoExists(repoURL string) bool {
	repoURL = fmt.Sprintf("https://gitlab.com/%s.git", repoURL)
	resp, err := http.Head(repoURL)
	if err != nil {
		return false
	}
	defer resp.Body.Close()

	return resp.StatusCode == http.StatusOK
}

func checkCodeowners(git *gitlab.Client, repo string) (bool, error) {
	codeOwnerPaths := []string{".gitlab/CODEOWNERS", "CODEOWNERS", "docs/CODEOWNERS"}

	for _, p := range codeOwnerPaths {
		set, err := checkCodeownerFile(git, repo, p)
		if err != nil {
			return false, fmt.Errorf("Could not check CODEOWNERS file %s: %w", p, err)
		}
		if set {
			return true, nil
		}
	}

	return false, nil
}

func checkCodeownerFile(git *gitlab.Client, repo string, fileName string) (bool, error) {
	file, response, err := git.RepositoryFiles.GetFile(repo, fileName, &gitlab.GetFileOptions{Ref: gitlab.String("main")})
	if response.StatusCode == 404 {
		return false, nil
	}
	if err != nil {
		return false, err
	}

	content, err := base64.StdEncoding.DecodeString(file.Content)
	if err != nil {
		return false, err
	}
	if !strings.Contains(string(content), "* @gitlab-com/runner-maintainers") {
		return false, nil
	}

	return true, nil
}

func checkRequestAccess(project *gitlab.Project) bool {
	return !project.RequestAccessEnabled
}

func checkGroupAccessLevel(project *gitlab.Project) bool {
	for _, m := range project.SharedWithGroups {
		if m.GroupID == gitlabRunnerMaintainersGroupID && m.GroupAccessLevel >= requiredAccessLevel {
			return true
		}
	}
	return false
}
