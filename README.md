# all-our-repo-are-belong-to-us

![Tankuki All Your Base](tanuki-base.png)

Verify runner repository permissions and setup.

List of Runner SaaS repos: https://about.gitlab.com/handbook/engineering/development/ops/verify/runner-saas/#projects-we-maintain

Discussion: https://gitlab.com/gitlab-org/gitlab-runner/-/issues/30916#note_1359394513